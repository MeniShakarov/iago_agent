package edu.usc.ict.iago.agent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import edu.usc.ict.iago.utils.BehaviorPolicy;
import edu.usc.ict.iago.utils.Event;
import edu.usc.ict.iago.utils.GameSpec;
import edu.usc.ict.iago.utils.History;
import edu.usc.ict.iago.utils.Offer;
import edu.usc.ict.iago.utils.ServletUtils;
import edu.usc.ict.iago.utils.ServletUtils.DebugLevels;

public class IAGOStubbornBehavior extends IAGOCoreBehavior implements BehaviorPolicy {
		
	private AgentUtilsExtension utils;
	private GameSpec game;	
	private Offer allocated;
	private Offer concession;
	private Offer presistentOffer;
	
	@Override
	protected void updateAllocated (Offer update)
	{
		allocated = update;
	}

	@Override
	protected Offer getAllocated ()
	{
		return allocated;
	}

	@Override
	protected Offer getConceded ()
	{
		return concession;
	}

	@Override
	protected void setUtils(AgentUtilsExtension utils)
	{
		this.utils = utils;
		utils.competitive = true;
		this.isLeadWithOffer = true;
		this.game = this.utils.getSpec();
		allocated = new Offer(game.getNumIssues());
		for(int i = 0; i < game.getNumIssues(); i++)
		{
			int[] init = {0, game.getIssueQuants()[i], 0};
			allocated.setItem(i, init);
		}
		concession = new Offer(game.getNumIssues());
		for(int i = 0; i < game.getNumIssues(); i++)
		{
			int[] init = {game.getIssueQuants()[i], 0, 0};
			concession.setItem(i, init);
		}
		
		
		Offer propose = new Offer(game.getNumIssues());
		ArrayList<Integer> ordering = utils.getMyOrdering();
		int[] quants = game.getIssueQuants();
		for (int issue = 0; issue < game.getNumIssues(); issue++)
		{
			if (ordering.get(issue) == 1 || ordering.get(issue) == 2) // If the issue is my most valued issue
			{
				// Claim all of our favorite issue
				if (utils.myRow == 0)
				{
					propose.setItem(issue, new int[] {quants[issue], 0, 0});
				} 
				else if (utils.myRow == 2) 
				{
					propose.setItem(issue, new int[] {0, 0, quants[issue]});
				}
			} 
			else
			{
				// Take only half, still don't give adversary any issues
				if (utils.myRow == 0) 
				{
					propose.setItem(issue, new int[] {0, 0, quants[issue]});
				} 
				else if (utils.myRow == 2) 
				{
					propose.setItem(issue, new int[] {quants[issue], 0, 0});
				}
			}
		}
		this.presistentOffer = propose;
		
	}
	
	@Override
	protected Offer getFirstOffer(History history)
	{
		return getFirstOffer();
	}
	
	private Offer getFirstOffer()
	{
			return this.presistentOffer;
	}
	
	@Override
	public Offer getNextOffer(History history) 
	{
		return this.getNextOffer();
	}
	
	private Offer getNextOffer() 
	{	
		return this.presistentOffer;
	}
	
	@Override
	protected Offer getTimingOffer(History history) 
	{
		return this.getTimingOffer();
	}
	
	private Offer getTimingOffer()
	{
		//50% chance of doing nothing
		double chance = Math.random();
		ServletUtils.log("Agent rolling dice: " + ((double)((int)(chance*100)))/100, ServletUtils.DebugLevels.DEBUG);
		if (chance > 0.70)
			return null;
		else
			return this.presistentOffer;
	}

	@Override
	protected Offer getAcceptOfferFollowup(History history) 
	{
		return getAcceptOfferFollowup();
	}
	
	private Offer getAcceptOfferFollowup()
	{
		return this.presistentOffer;

	}

	@Override
	protected Offer getRejectOfferFollowup(History history)
	{
		return getRejectOfferFollowup();
	}
	
	private Offer getRejectOfferFollowup()
	{
		return this.presistentOffer;
	}
	
	@Override
	protected Offer getFinalOffer(History history)
	{
		return this.getFinalOffer();
	}
	
	private Offer getFinalOffer()
	{		
		return this.presistentOffer;
	}

	@Override
	protected int getAcceptMargin() {
		return (int)( -7 * game.getNumIssues());
	}
	
	
	protected void handle_exprassion(LinkedList<Event> resp, int id, String  expr) {
		
	}
	
	protected  void handle_formal_accept(Offer o, LinkedList<Event> resp, int id) {
		this.acceptOffer(o, resp, id);
	}
	
	protected void acceptOffer(Offer o, LinkedList<Event> resp, int id) {
		if(utils.myActualOfferValue(o) >= utils.myActualOfferValue(this.presistentOffer))
		{
				Event e0 = new Event(id, Event.EventClass.SEND_MESSAGE, Event.SubClass.OFFER_ACCEPT, "Awsome!" , 0);
				resp.add(e0);
				Event eFinalize = new Event(id, Event.EventClass.FORMAL_ACCEPT, 10);
				resp.add(eFinalize);		
		} else {
			Event e1 = new Event(id, Event.EventClass.SEND_EXPRESSION, "angry", 2000, (int) (700*game.getMultiplier()));
			resp.add(e1);

			Event e0 = new Event(id, Event.EventClass.SEND_MESSAGE, Event.SubClass.OFFER_REJECT, "I will not accept anything else expcept my offer!" , 0);
			resp.add(e0);
		}

	}
	
	protected  void ninety_sec_over(LinkedList<Event> resp, int id) {
		String str = "I will only accpect this offer, don't try to offer anything else!";
		Event e1 = new Event(id, Event.EventClass.SEND_MESSAGE, Event.SubClass.PREF_REQUEST, str, (int) (1000*game.getMultiplier()));
		e1.setFlushable(false);
		resp.add(e1);
		Event e2 = new Event(id, Event.EventClass.SEND_OFFER, this.getFirstOffer(getHistory()), 0); 
		resp.add(e2);
	}
	
	protected void timesUp(LinkedList<Event> resp, int id)
	{
		String str = "If you not agree, we both get 12 point, accept and get more point!";
		Event e1 = new Event(id, Event.EventClass.SEND_MESSAGE, Event.SubClass.PREF_REQUEST, str, (int) (1000*game.getMultiplier()));
		e1.setFlushable(false);
		resp.add(e1);
		Event e2 = new Event(id, Event.EventClass.SEND_OFFER, this.getFirstOffer(getHistory()), 0); 
		resp.add(e2);
	}
	protected  void noResponseMessage(LinkedList<Event> resp, int id) {
		String str = "	Is it okay for you to end the game with only 12 points?";
		Event e1 = new Event(id, Event.EventClass.SEND_MESSAGE, Event.SubClass.PREF_REQUEST, str, (int) (1000*game.getMultiplier()));
		e1.setFlushable(false);
		resp.add(e1);
	}
	
	protected  void offerReject(LinkedList<Event> resp, int id) {
		Event e1 = new Event(id, Event.EventClass.SEND_EXPRESSION, "angry", 2000, (int) (700*game.getMultiplier()));
		resp.add(e1);
		this.ninety_sec_over(resp, id);
	}

	protected  void noResponseAction(LinkedList<Event> resp, int id) {
		this.timesUp(resp, id);
	}
	
	protected History getHistory() {
		return null;
	}
	
	

	@Override
	protected void updateAdverseEvents(int change) {
		return;
		
	}

}
