package edu.usc.ict.iago.agent;

import java.util.ArrayList;

import edu.usc.ict.iago.utils.BehaviorPolicy;
import edu.usc.ict.iago.utils.GameSpec;
import edu.usc.ict.iago.utils.History;
import edu.usc.ict.iago.utils.Offer;
import edu.usc.ict.iago.utils.ServletUtils;
import java.util.concurrent.ThreadLocalRandom;


public class IAGOChangingBehavior extends IAGOCoreBehavior implements BehaviorPolicy {

	private AgentUtilsExtension utils;
	private GameSpec game;
	private Offer allocated;
	private Offer concession;
	private ArrayList<IAGOCoreBehavior> behaviors;
	
	public IAGOChangingBehavior() {
		this.behaviors = new ArrayList<IAGOCoreBehavior>();
		this.behaviors.add(new RepeatedFavorBehavior(RepeatedFavorBehavior.LedgerBehavior.FAIR));
		this.behaviors.add(new RepeatedFavorBehavior(RepeatedFavorBehavior.LedgerBehavior.BETRAYING));
		this.behaviors.add(new RepeatedFavorBehavior(RepeatedFavorBehavior.LedgerBehavior.LIMITED));
		this.behaviors.add(new IAGOCompetitiveBehavior());
		this.behaviors.add(new IAGOBuildingBehavior());
		this.behaviors.add(new IAGOConcedingBehavior());
	}
	
	@Override
	protected void updateAllocated(Offer update) {
		allocated = update;
	}

	@Override
	protected Offer getAllocated() {
		return allocated;
	}

	@Override
	protected Offer getConceded() {
		return concession;
	}

	@Override
	protected void setUtils(AgentUtilsExtension utils) {
		this.utils = utils;

		this.game = this.utils.getSpec();
		allocated = new Offer(game.getNumIssues());
		for (int i = 0; i < game.getNumIssues(); i++) {
			int[] init = { 0, game.getIssueQuants()[i], 0 };
			allocated.setItem(i, init);
		}
		concession = new Offer(game.getNumIssues());
		for (int i = 0; i < game.getNumIssues(); i++) {
			int[] init = { game.getIssueQuants()[i], 0, 0 };
			concession.setItem(i, init);
		}
	}

	@Override
	protected Offer getFinalOffer(History history) {
		int index = ThreadLocalRandom.current().nextInt(0, 7);
		return this.behaviors.get(index).getFinalOffer(history);
		
	}

	@Override
	public Offer getNextOffer(History history) {
		int index = ThreadLocalRandom.current().nextInt(0, 7);
		return this.behaviors.get(index).getNextOffer(history);
	}

	@Override
	protected int getAcceptMargin() {
		int index = ThreadLocalRandom.current().nextInt(0, 7);
		return this.behaviors.get(index).getAcceptMargin();
		
	}

	@Override
	protected Offer getTimingOffer(History history) {
		int index = ThreadLocalRandom.current().nextInt(0, 7);
		return this.behaviors.get(index).getTimingOffer(history);
	}

	@Override
	protected Offer getAcceptOfferFollowup(History history) {
		ArrayList<Integer> ordering = utils.getMyOrdering();

		// start from where we currently have conceded
		Offer propose = new Offer(game.getNumIssues());
		for (int issue = 0; issue < game.getNumIssues(); issue++)
			propose.setItem(issue, concession.getItem(issue));

		int[] free = new int[game.getNumIssues()];
		for (int issue = 0; issue < game.getNumIssues(); issue++) {
			free[issue] = concession.getItem(issue)[1];
		}

		// find top deals
		int topVH = -1;
		int max = game.getNumIssues() + 1;
		max = game.getNumIssues() + 1;
		for (int i = 0; i < game.getNumIssues(); i++)
			if (free[i] > 0 && ordering.get(i) < max) {
				topVH = i;
				max = ordering.get(i);
			}

		if (topVH == -1) // we're already at a full offer, but need to try something different
		{
			// just repeat and keep allocated
		} else
			propose.setItem(topVH,
					new int[] { concession.getItem(topVH)[0] + free[topVH], 0, concession.getItem(topVH)[2] }); // take
																												// it
																												// all
		concession = propose;

		return propose;
	}

	@Override
	protected Offer getFirstOffer(History history) {
		Offer propose = new Offer(game.getNumIssues());
		ArrayList<Integer> ordering = utils.getMyOrdering();
		int[] quants = game.getIssueQuants();
		for (int issue = 0; issue < game.getNumIssues(); issue++) {
			if (ordering.get(issue) == 1)
				propose.setItem(issue, new int[] { quants[issue], 0, 0 });// claim all of our best issue
			else
				propose.setItem(issue, new int[] { (int) ((quants[issue] / 2.0) + .5),
						quants[issue] - (int) ((quants[issue] / 2.0) + .5), 0 });// claim half of all remaining issues
		}
		concession = propose;
		return propose;
	}

	@Override
	protected Offer getRejectOfferFollowup(History history) {

		// start from where we currently have conceded
		Offer propose = new Offer(game.getNumIssues());
		for (int issue = 0; issue < game.getNumIssues(); issue++)
			propose.setItem(issue, concession.getItem(issue));

		int[] free = new int[game.getNumIssues()];
		int totalFree = 0;
		for (int issue = 0; issue < game.getNumIssues(); issue++) {
			free[issue] = concession.getItem(issue)[1];
			totalFree += concession.getItem(issue)[1];
		}

		if (totalFree > 0) // concede free items
		{
			for (int issue = 0; issue < game.getNumIssues(); issue++) {
				propose.setItem(issue,
						new int[] { concession.getItem(issue)[0], 0, concession.getItem(issue)[2] + free[issue] });
			}
		} else
			return null;

		concession = propose;

		return propose;
	}

	@Override
	protected void updateAdverseEvents(int change) {
		return;

	}

}
