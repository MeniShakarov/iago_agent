package edu.usc.ict.iago.agent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import edu.usc.ict.iago.utils.BehaviorPolicy;
import edu.usc.ict.iago.utils.Event;
import edu.usc.ict.iago.utils.GameSpec;
import edu.usc.ict.iago.utils.History;
import edu.usc.ict.iago.utils.Offer;
import edu.usc.ict.iago.utils.ServletUtils;
import edu.usc.ict.iago.utils.ServletUtils.DebugLevels;

public class IAGOStubbornBehavior extends IAGOCoreBehavior implements BehaviorPolicy {
		
	private AgentUtilsExtension utils;
	private GameSpec game;	
	private Offer allocated;
	private Offer concession;
	private Offer presistentOffer;
	public Boolean isLeadWithOffer;
	
	@Override
	protected void updateAllocated (Offer update)
	{
		allocated = update;
	}

	@Override
	protected Offer getAllocated ()
	{
		return allocated;
	}

	@Override
	protected Offer getConceded ()
	{
		return concession;
	}

	@Override
	protected void setUtils(AgentUtilsExtension utils)
	{
		this.utils = utils;
		utils.competitive = true;
		
		this.game = this.utils.getSpec();
		allocated = new Offer(game.getNumIssues());
		for(int i = 0; i < game.getNumIssues(); i++)
		{
			int[] init = {0, game.getIssueQuants()[i], 0};
			allocated.setItem(i, init);
		}
		concession = new Offer(game.getNumIssues());
		for(int i = 0; i < game.getNumIssues(); i++)
		{
			int[] init = {game.getIssueQuants()[i], 0, 0};
			concession.setItem(i, init);
		}
		
		
		Offer propose = new Offer(game.getNumIssues());
		ArrayList<Integer> ordering = utils.getMyOrdering();
		int[] quants = game.getIssueQuants();
		for (int issue = 0; issue < game.getNumIssues(); issue++)
		{
			if (ordering.get(issue) == 1 || ordering.get(issue) == 2) // If the issue is my most valued issue
			{
				// Claim all of our favorite issue
				if (utils.myRow == 0)
				{
					propose.setItem(issue, new int[] {quants[issue], 0, 0});
				} 
				else if (utils.myRow == 2) 
				{
					propose.setItem(issue, new int[] {0, 0, quants[issue]});
				}
			} 
			else
			{
				// Take only half, still don't give adversary any issues
				if (utils.myRow == 0) 
				{
					propose.setItem(issue, new int[] {0, 0, quants[issue]});
				} 
				else if (utils.myRow == 2) 
				{
					propose.setItem(issue, new int[] {quants[issue], 0, 0});
				}
			}
		}
		this.presistentOffer = propose;
		
	}
	
	@Override
	protected Offer getFirstOffer(History history)
	{
		return getFirstOffer();
	}
	
	private Offer getFirstOffer()
	{
			return this.presistentOffer;
	}
	
	@Override
	public Offer getNextOffer(History history) 
	{
		return this.getNextOffer();
	}
	
	private Offer getNextOffer() 
	{	
		return this.presistentOffer;
	}
	
	@Override
	protected Offer getTimingOffer(History history) 
	{
		return this.getTimingOffer();
	}
	
	private Offer getTimingOffer()
	{
		//50% chance of doing nothing
		double chance = Math.random();
		ServletUtils.log("Agent rolling dice: " + ((double)((int)(chance*100)))/100, ServletUtils.DebugLevels.DEBUG);
		if (chance > 0.70)
			return null;
		else
			return this.presistentOffer;
	}

	@Override
	protected Offer getAcceptOfferFollowup(History history) 
	{
		return getAcceptOfferFollowup();
	}
	
	private Offer getAcceptOfferFollowup()
	{
		return this.presistentOffer;

	}

	@Override
	protected Offer getRejectOfferFollowup(History history)
	{
		return getRejectOfferFollowup();
	}
	
	private Offer getRejectOfferFollowup()
	{
		return this.presistentOffer;
	}
	
	@Override
	protected Offer getFinalOffer(History history)
	{
		return this.getFinalOffer();
	}
	
	private Offer getFinalOffer()
	{		
		return this.presistentOffer;
	}

	@Override
	protected int getAcceptMargin() {
		return (int)( -7 * game.getNumIssues());
	}
	
	/**
	 * Determines whether the agent should accept an offer based on the joint value using perceived user preferences
	 * @param o an offer to evaluate
	 * @return a boolean, true if the offer provides the agent with at least a given percentage of the overall joint value 
	 */
//	protected boolean acceptOffer(Offer o)
//	{
//		double myValue, opponentValue, jointValue;
//		myValue = utils.myActualOfferValue(o);
//		opponentValue = utils.getAdversaryValue(o);
//		ServletUtils.log("acceptOffer method - Agent Value: " + myValue + ", Perceived Opponent Value: " + opponentValue + ", Opponent BATNA: " + utils.adversaryBATNA, ServletUtils.DebugLevels.DEBUG);
//		jointValue = myValue + opponentValue;
//		return (myValue/jointValue > .7 & myValue >= utils.myPresentedBATNA) || 
//			   (!utils.conflictBATNA(utils.myPresentedBATNA, utils.adversaryBATNA) && myValue >= utils.myPresentedBATNA);	//The threshold of .6 is semi-arbitrary
//	}
	
	protected void acceptOffer(Offer o, LinkedList<Event> resp, int id) {
		if(utils.myActualOfferValue(o) >= utils.myActualOfferValue(this.presistentOffer))
		{
				Event e0 = new Event(id, Event.EventClass.SEND_MESSAGE, Event.SubClass.OFFER_ACCEPT, "Awsome!" , 0);
				resp.add(e0);
				Event eFinalize = new Event(id, Event.EventClass.FORMAL_ACCEPT, 10);
				resp.add(eFinalize);		
		}

	}
	
	protected History getHistory() {
		return null;
	}
	

	@Override
	protected void updateAdverseEvents(int change) {
		return;
		
	}

}
